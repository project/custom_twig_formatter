<?php

namespace Drupal\custom_twig_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'custom_twig_markup' formatter.
 *
 * @FieldFormatter(
 *   id = "custom_twig_markup",
 *   label = @Translation("Custom Twig markup"),
 *   field_types = {
 *     "boolean",
 *     "changed",
 *     "created",
 *     "comment",
 *     "daterange",
 *     "datetime",
 *     "decimal",
 *     "entity_reference",
 *     "email",
 *     "file",
 *     "file_uri",
 *     "image",
 *     "integer",
 *     "language",
 *     "link",
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *     "float",
 *     "string",
 *     "string_long",
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *     "telephone",
 *     "timestamp",
 *     "uri"
 *   }
 * )
 */
class CustomTwigFormatter extends FormatterBase {


  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Twig service.
   *
   * @var \Drupal\Core\Template\TwigEnvironment
   */
  protected $twig;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  public $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->twig = $container->get('twig');
    $instance->moduleHandler = $container->get('module_handler');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'custom_twig_markup' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['custom_twig_markup'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Twig code'),
      '#description' => $this->t('Enter the Twig code that will be evaluated., e.g: @data, see all available replacement patterns under. You can also use <a href="https://www.drupal.org/docs/theming-drupal/twig-in-drupal/functions-in-twig-templates" target=”_blank”>Twig functions</a> and <a href="https://www.drupal.org/docs/theming-drupal/twig-in-drupal/filters-modifying-variables-in-twig-templates" target=”_blank”>Twig filters</a>.', ['@data' => "<code>{{ label }}</code>"]),
      '#default_value' => $this->getSetting('custom_twig_markup'),
    ];

    // Generate help text for replacement patterns.
    $fieldDefinitions = [
      $this->fieldDefinition->getName() => $this->fieldDefinition,
    ];

    // Default - current field.
    $fields = [
      $this->fieldDefinition->getName() => $this->fieldDefinition,
    ];

    $entity_type = $this->fieldDefinition->getTargetEntityTypeId();
    $bundle = $this->fieldDefinition->getTargetBundle();

    if ($bundle) {
      $storage = $this->entityTypeManager->getStorage($entity_type);
      $entity = $storage
        ->create([$storage->getEntityType()->getKey('bundle') => $bundle]);
      $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
      $fields = $entity->getFields();
    }

    $fieldsDoc = [];
    foreach ($fieldDefinitions as $field_id => $value) {
      $type = $value->getType();
      $fieldsDoc[$field_id]['type'] = $type;
    }

    foreach ($fields as $field_id => $value) {
      if ($value instanceof FieldDefinitionInterface) {
        $fieldsDoc[$field_id]['class'] = $value->getClass();
        continue;
      }
      $fieldsDoc[$field_id]['class'] = $value->getFieldDefinition()->getClass();
    }
    $tokensDoc = [
      'label'             => 'string',
    ];

    // Build documentation.
    $field_name           = $this->fieldDefinition->getName();
    $tokens_current_field = '<dl>';
    $tokens_all           = '<dl>';
    foreach ($fieldsDoc as $id => $value) {
      $type = $value['type'];
      $class = $value['class'];
      if ($id != $field_name) {
        $tokens_all .= "<dt>{{ $id }}  <em>$class</em>  </dt><dd>$type field.</dd></dt>";
      }
      else {
        $tokens_current_field .= "<dt>{{ $id }}  <em>$class</em>  </dt><dd>$type field.</dd></dt>";
      }
    }
    foreach ($tokensDoc as $id => $type) {
      $id_name = str_replace('_', ' ', $id);
      $tokens_current_field .= "<dt>{{ $id }}  <em>$type</em> </dt><dd>The field $id_name.</dd></dt>";
    }
    $tokens_current_field .= '<dl>';
    $tokens_all           .= '</dl>';

    $form['tokens_current_field'] = [
      '#type' => 'details',
      '#title' => $this->t('Replacement patterns for this field'),
      '#open' => TRUE,
      '#markup' => $tokens_current_field,
    ];

    if ($bundle) {
      $form['tokens_all'] = [
        '#type' => 'details',
        '#title' => $this->t('Replacement patterns for other fields'),
        '#open' => FALSE,
        '#markup' => $tokens_all,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $entity = $items->getEntity();
    $field_name = $this->fieldDefinition->getName();
    $field_type = $this->fieldDefinition->getType();
    $label_display_position = $this->label;
    // field_display_label module integration.
    $field_display_label_module = $this->moduleHandler->moduleExists('field_display_label');
    if ($field_display_label_module) {
      $display_label_setting = $this->fieldDefinition->getThirdPartySetting('field_display_label', 'display_label');
      if (isset($display_label_setting) && !empty($display_label_setting)) {
        $label = $display_label_setting;
      }
      else {
        $label = $this->fieldDefinition->getLabel();
      }
    }
    else {
      $label = $this->fieldDefinition->getLabel();
    }

    // Load twig field replacement patterns.
    $context = [];
    foreach ($entity->getFields() as $field_id => $value) {
      $context[$field_id] = $value;
    }
    $context['label'] = $label;

    // Build twig markup.
    $markup = '';
    try {
      $markup = $this->twig->createTemplate($this->getSetting('custom_twig_markup'))
        ->render($context);
    }
    catch (Twig_Error $error) {
      $this->messenger()->addError($error->getMessage());
    }

    $entity_type = $entity->getEntityTypeId();
    $elements = [
      '#type' => 'markup',
      '#markup' => $markup,
      '#view_mode' => $this->viewMode,
      '#language' => $items->getLangcode(),
      '#field_name' => $field_name,
      '#field_type' => $field_type,
      '#field_translatable' => $this->fieldDefinition->isTranslatable(),
      '#entity_type' => $entity_type,
      '#bundle' => $entity->bundle(),
      '#object' => $entity,
      '#items' => $items,
      '#formatter' => $this->getPluginId(),
      '#is_multiple' => $this->fieldDefinition->getFieldStorageDefinition()->isMultiple(),
      '#third_party_settings' => $this->getThirdPartySettings(),
    ];
    // @todo Attaching libraries for all fields in a generic way here.
    if ($field_type == 'file') {
      $elements['#attached']['library'][] = "file/drupal.file";
    }

    return [$elements];
  }

}
